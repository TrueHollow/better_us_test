const express = require('express');
const config = require('config');

const app = express();
const logger = require('./common/Logger')('index.js');

const middleware = require('./middleware');

middleware(app, logger);

const api = require('./api/v1');

api(app);

module.exports = app;

const httpPort = process.env.PORT || config.get('http_server.http_port');

const { preInit } = require('./db');

preInit.then(() => {
  app.listen(httpPort);
  logger.info(`app running on port http://localhost:${httpPort} ...`);
});
