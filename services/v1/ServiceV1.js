const db = require('../../db');
const Participants = require('./participants');

class ServiceV1 {
  constructor() {
    this.participants = new Participants(db);
  }

  get Participants() {
    return this.participants;
  }
}

module.exports = ServiceV1;
