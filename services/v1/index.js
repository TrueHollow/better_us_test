const ServiceV1 = require('./ServiceV1');

let singleton;

module.exports = (Service = ServiceV1) => {
  if (!singleton) {
    singleton = new Service();
  }
  return singleton;
};
