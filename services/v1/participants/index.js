class Participants {
  constructor(datasource) {
    const { Participant, Status } = datasource;
    this.Participant = Participant;
    this.Status = Status;
  }

  async getAll() {
    const { Participant } = this;
    return Participant.find();
  }

  async getAccepted() {
    const { Participant, Status } = this;
    const status = await Status.findOne({ name: 'accepted' }, '_id');
    const { _id } = status;
    return Participant.find({ statusId: _id });
  }

  async getById(id) {
    const { Participant } = this;
    return Participant.findOne({ _id: id }, '-_id');
  }

  async updateStatus(id, { status }) {
    const { Participant, Status } = this;
    const [statusDocument, participantDocument] = await Promise.all([
      Status.findOne({ name: status }, '_id'),
      Participant.findOne({ _id: id }),
    ]);
    if (!statusDocument) {
      return { err: 'Unknown status' };
    }
    if (!participantDocument) {
      return { err: 'Participant not found' };
    }
    const { _id } = statusDocument;
    participantDocument.statusId = _id;
    await participantDocument.save();
    return { status: 'ok' };
  }
}

module.exports = Participants;
