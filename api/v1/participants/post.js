const logger = require('../../../common/Logger')('api/v1/participants/post.js');
const service = require('../../../services/v1')();

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { Participants } = service;
    const participant = await Participants.getById(id);
    return res.json(participant);
  } catch (e) {
    logger.error(e);
    return res.status(500).json({ success: false, err: 'internal error' });
  }
};
