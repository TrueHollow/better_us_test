const logger = require('../../../common/Logger')('api/v1/participants/get.js');
const service = require('../../../services/v1')();

module.exports = async (req, res) => {
  try {
    const { Participants } = service;
    const allParticipants = await Participants.getAccepted();
    return res.json(allParticipants);
  } catch (e) {
    logger.error(e);
    return res.status(500).json({ success: false, err: 'internal error' });
  }
};
