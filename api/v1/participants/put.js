const logger = require('../../../common/Logger')('api/v1/participants/put.js');
const service = require('../../../services/v1')();

module.exports = async (req, res) => {
  try {
    const { id } = req.params;
    const { body } = req;
    const { Participants } = service;
    const operation = await Participants.updateStatus(id, body);
    if (operation.err) {
      return res.status(400).json(operation);
    }
    return res.json(operation);
  } catch (e) {
    logger.error(e);
    return res.status(500).json({ success: false, err: 'internal error' });
  }
};
