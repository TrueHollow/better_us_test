const getHandler = require('./get');
const postHandler = require('./post');
const putHandler = require('./put');

module.exports = app => {
  app.route('/api/v1/participants').get(getHandler);
  app
    .route('/api/v1/participants/:id')
    .post(postHandler)
    .put(putHandler);
};
