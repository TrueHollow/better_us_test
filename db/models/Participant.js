module.exports = mongoose => {
  const { Schema } = mongoose;
  const productSchema = new Schema({
    firstName: String,
    lastName: String,
    phoneNumber: String,
    email: String,
    gender: String,
    weight: Number,
    height: Number,
    zip: Number,
    statusId: { type: Schema.Types.ObjectId, ref: 'Status' },
  });

  const Status = mongoose.model('Status');

  // eslint-disable-next-line func-names
  const preSaveMiddleware = async function() {
    if (!this.statusId) {
      const statusDoc = await Status.findOne({ name: 'pending' }, '_id');
      const { _id } = statusDoc;
      this.statusId = _id;
    }
  };

  productSchema.pre('save', preSaveMiddleware);

  return mongoose.model('Participant', productSchema);
};
