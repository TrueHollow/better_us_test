module.exports = mongoose => {
  const { Schema } = mongoose;
  const statusSchema = new Schema({
    name: String,
  });

  return mongoose.model('Status', statusSchema);
};
