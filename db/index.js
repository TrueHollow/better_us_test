const mongoose = require('mongoose');
const config = require('config');
const logger = require('../common/Logger')('src/db/index.js');

const mongooseConfig = config.get('mongoose');
mongoose.connect(mongooseConfig.uri, mongooseConfig.options);

const db = mongoose.connection;
db.on('error', err => {
  logger.error('Mongoose error:', err);
});
db.once('open', () => {
  logger.info('Connected to mongo.');
});

const Status = require('./models/Status')(mongoose);
const Participant = require('./models/Participant')(mongoose);

const preInit = async () => {
  const any = await Status.findOne();
  if (!any) {
    await Status.insertMany([
      { name: 'pending' },
      { name: 'accepted' },
      { name: 'rejected' },
    ]);
  }
};

module.exports = {
  mongoose,
  Status,
  Participant,
  preInit: preInit(),
};
