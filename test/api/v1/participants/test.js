const chai = require('chai');
chai.use(require('chai-http'));
const { expect } = require('chai');
const app = require('../../../../index');
const db = require('../../../../db');

let id;

const testParticipant = {
  firstName: 'Anatoly',
  lastName: 'Russkih',
  phoneNumber: '1234567890',
  email: 'deimos_and_fobos@mail.ru',
  gender: 'male',
  weight: -1,
  height: -1,
  zip: 1234567,
};

const urlPath = '/api/v1/participants';
describe('Participants test', () => {
  before(async () => {
    const { Participant, preInit } = db;
    await preInit;
    const testObject = await Participant.create(testParticipant);
    // eslint-disable-next-line no-underscore-dangle
    id = testObject._id;
  });
  after(async () => {
    const { Participant } = db;
    return Participant.deleteOne(testParticipant);
  });
  it('test GET accepted request', async () => {
    const res = await chai.request(app).get(urlPath);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body).to.be.an('array');
  });
  it('test POST request', async () => {
    const res = await chai.request(app).post(`${urlPath}/${id}`);
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body).to.be.an('object');
  });
  it('test PUT request', async () => {
    const res = await chai
      .request(app)
      .put(`${urlPath}/${id}`)
      .send({ status: 'accepted' });
    expect(res).to.have.status(200);
    const { body } = res;
    expect(body).to.be.an('object');
    expect(body).to.have.property('status', 'ok');
  });
});
